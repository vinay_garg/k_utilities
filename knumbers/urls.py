from django.conf.urls import url
from django.conf.urls import patterns, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'search', views.process, name='process'),
    url(r'^', views.index, name='index'),
]