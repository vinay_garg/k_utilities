from django.http import HttpResponse
from django.shortcuts import render
import requests, json


def index(request):
    return render(request, 'knumbers/form.html')

ACCESS_KEY = 'b73dd6bc-140e-4f26-b98a-1ba3dfb68ce6'


def get_result(x):
    yield x


def process(request):
    import ipdb; ipdb.set_trace()
    if request.method == 'POST':
        final_result = []
        html = ""
        user_input = request.POST.get('user_number', None)
        try:
            get_api = 'http://krm.knowlarity.com/v1/number/{}/?access_key={}'.format(user_input, ACCESS_KEY)
            get_details = requests.get(get_api, timeout=60)
            content = json.loads(get_details.content)
            if get_details.status_code == 200:
                ivr_id = content['allocation_ivr_ref_num']
                number_state = content['number_state']
                if number_state == 'Reserved':
                    delete_api = 'http://krm.knowlarity.com/v1/number/{}/allocation/?access_key={}&ivr_id={}'\
                        .format(user_input, ACCESS_KEY, ivr_id)
                    result = requests.delete(delete_api, timeout=60)
                    if result.status_code == 200:
                        html = '<h1> {} '.format(result.content)
                else:
                    html = '{} number not in Reserved state, current state: {}'.format(user_input ,number_state)
            else:
                final_result.append('Some error occurred in fetching detail from KRM')
        except:
            html = '<h1> Some exception Occurred <h1>'
        return HttpResponse(html)
    else:
        return HttpResponse('<label> Please provide a number to release </label>')